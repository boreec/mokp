CC=g++
CXXFLAGS=-Wall -g -ggdb
SRC_DIR=src
TARGETS=main

main: $(SRC_DIR)/Knapsack.o $(SRC_DIR)/KnapsackSolution.o $(SRC_DIR)/Item.o $(SRC_DIR)/DataParser.o $(SRC_DIR)/main.o $(SRC_DIR)/Heuristic.o $(SRC_DIR)/Filter.o $(SRC_DIR)/SolutionGenerator.o $(SRC_DIR)/HillClimbing.o
	$(CC) -o $@ $^

$(SRC_DIR)/main.o: $(SRC_DIR)/main.cpp
	$(CC) -c $^ $(CXXFLAGS) -o $@

$(SRC_DIR)/Filter.o: $(SRC_DIR)/Filter.cpp
	$(CC) -c $^ $(CXXFLAGS) -o $@

$(SRC_DIR)/Knapsack.o: $(SRC_DIR)/Knapsack.cpp
	$(CC) -c $^ $(CXXFLAGS) -o $@

$(SRC_DIR)/KnapsackSolution.o: $(SRC_DIR)/KnapsackSolution.cpp
	$(CC) -c $^ $(CXXFLAGS) -o $@

$(SRC_DIR)/Item.o: $(SRC_DIR)/Item.cpp
	$(CC) -c $^ $(CXXFLAGS) -o $@

$(SRC_DIR)/Heuristic.o: $(SRC_DIR)/Heuristic.cpp
	$(CC) -c $^ $(CXXFLAGS) -o $@

$(SRC_DIR)/HillClimbing.o: $(SRC_DIR)/HillClimbing.cpp
	$(CC) -c $^ $(CXXFLAGS) -o $@

$(SRC_DIR)/SolutionGenerator.o: $(SRC_DIR)/SolutionGenerator.cpp
	$(CC) -c $^ $(CXXFLAGS) -o $@

$(SRC_DIR)/DataParser.o: $(SRC_DIR)/DataParser.cpp
	$(CC) -c $^ $(CXXFLAGS) -o $@

clean:
	rm -f $(SRC_DIR)/*.o

cleandat:
	rm -f *.dat

cleangnu:
	rm -f *.gnu

cleanall: clean cleandat cleangnu
	rm -f $(TARGETS)
