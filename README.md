# Multi-objective knapsack problem (MOKP)

author : Cyprien Borée

see http://boreec.fr

## Data

Data used for instances are described in "E. Zitzler, M. Laumanns, and L. Thiele. SPEA2: Improving the Strength Pareto Evolutionary Algorithm. Technical Report 103, Computer Engineering and Communication Networks Lab (TIK), Swiss Federal Institute of Technology (ETH) Zurich, Gloriastrasse 35, CH-8092 Zurich, May 2001" and were downloaded from https://sop.tik.ee.ethz.ch/download/supplementary/testProblemSuite.

## How to use

### Compilation

The program is built with `C++`, so make sure to have a suitable compiler to execute it. No cross-platforms has been made, it only works on linux. Also the automation tool is makefile :
```bash
$ make # build all executable
$ make clean # remove the object files
* make cleandat # remove the .dat files
$ make cleanall # remove all of the previous files at once.
```

### Execution

The program takes the following arguments :

- **-b UNSIGNED_VALUE** : Generate a certain amount of solutions as a base.
- **-D** : For every knapsacks loaded, the results of solutions are exported in a dedicated .dat file (sol_X.dat) and a .gnu file(sol_X.gnu).
- **-d INSTANCE_FILE** : Load an instance file, especially one from `data` folder. This argument is mandatory.
- **-f FILTER** : Filter out solutions from base solutions. The available filters are "OVERWEIGHT", "PARETO_OPTIMAL" and "DUPLICATE".
- **-h** : Display this argument list and description into stdout.
- **-M UNSIGNED_VALUE** : Specify a limit for the iterations during heuristic searchs.
- **-p** : If **-d** is specified, the loaded data in printed into stdout.

To display the solution results :
```bash
$ ./main -d data/knapsack_100_2 -D -b 100000
$ gnuplot -persist sol_0.gnu
```
![output_example](data/img/100000_random_solutions.png)

To filter out the duplicate solutions (can be very rare depending on the solution's size) :
```bash
$ ./main -d data/knapsack_100_2 -b 30000 -f DUPLICATE -D
$ gnuplot -persist sol_O.gnu
```

To filter out the overweight solutions :
```bash
$ ./main -d data/knapsack_100_2 -b 100000 -D -f OVERWEIGHT
$ gnuplot -persist sol_0.gnu
```

![output_example2](data/img/overweight_solutions_filtered.png)

To filter out non-Pareto optimal solutions :
```bash
$ ./main -d data/knapsack_100_2 -b 1000000 -f PARETO_OPTIMAL -D
$ gnuplot -persist sol_0.gnu
```

![output_example3](data/img/nonpareto_solutions_filtered.png)

The filters can be combined together. For example to filter out the overweight solutions and the non-Pareto optimal solutions :
```bash
$ ./main -d data/knapsack_100_2 -b 1000000 -f PARETO_OPTIMAL -f OVERWEIGHT -D
$ gnuplot -persist sol_0.gnu
```

![output_example4](data/img/overweight_and_nonpareto_solutions_filtered.png)