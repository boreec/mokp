#ifndef __SOLUTION_GENERATOR_HPP__
#define __SOLUTION_GENERATOR_HPP__
#include <random>
#include <vector>
#include "Knapsack.hpp"
#include "KnapsackSolution.hpp"
/*
 * This class generates solutions as a starting point for
 * heuristics. They are generated based on a probability : each
 * bit has a chance to be 1, 0 otherwise.
 * The solutions are not checked in this class, it means that among
 * generated solutions there are duplicates and/or solutions exceeding
 * a knapsack capacity.
 * */
class SolutionGenerator{
private:
  
  /*
   * The knapsack corresponding to the solution. The solution has the same
   * size as the knapsack can contain items.
   * */
  const Knapsack& _knapsack;

public:

  /*
   * Construct a solution on a given knapsack.
   * */
  SolutionGenerator(const Knapsack& k);

  /*
   * Generate a certain amount of solutions randomly.
   * probability : The probability for a bit to be a 1, 0 otherwise.
   * quantity : The amount of solutions to generate.
   * solutions : A reference to the generated solutions.
   * */
  void generate_randomly(const float probability, const unsigned quantity, std::vector<KnapsackSolution>& solutions);
};
#endif
