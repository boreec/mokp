#ifndef __HEURISTIC_HPP__
#define __HEURISTIC_HPP__
#include <vector>
#include "Knapsack.hpp"
#include "KnapsackSolution.hpp"

/*
 * Base class for heuristic.
 * */
class Heuristic{
protected:
  /*
   * The knapsack on which the heuristic tries to minimize weight
   * and maximize profit.
   * */
  const Knapsack& _knapsack;
public:
  /*
   * Construct the current heuristic on a given knapsack.
   * */
  Heuristic(const Knapsack& k);

  /*
   * Base class destructor must be implemented by derived classes.
   * */
  virtual ~Heuristic();
};
#endif
