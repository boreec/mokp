#include "Filter.hpp"

std::vector<KnapsackSolution> Filter::remove_overweight(const std::vector<KnapsackSolution>& solutions){
  std::vector<KnapsackSolution> result;

  for(unsigned i = 0; i < solutions.size(); ++i){
    if(!solutions[i].exceed_capacity()){
      result.push_back(solutions[i]);
    }
  }
  return result;
}

std::vector<KnapsackSolution> Filter::remove_duplicates(const std::vector<KnapsackSolution>& solutions){
  std::vector<KnapsackSolution> result;
  std::vector<bool> duplicate_idx(solutions.size(), false);
  
  for(unsigned i = 0; i < solutions.size(); ++i){
    if(duplicate_idx[i])
      continue;
    
    unsigned j = i + 1;
    while(j < solutions.size() && solutions[i] != solutions[j]){
      j++;
    }

    if(j < solutions.size()){
      duplicate_idx[j] = true;
    }else{
      result.push_back(solutions[i]);
    }
  }
  return result;
}

std::vector<KnapsackSolution> Filter::keep_pareto_optimal(const std::vector<KnapsackSolution>& solutions){
  std::vector<KnapsackSolution> result;

  std::vector<bool> visited_solutions(solutions.size(), false);
  
  for(unsigned i = 0; i < solutions.size(); ++i){
    visited_solutions[i] = true;
    
    unsigned j = 0;
    // Loop over solutions as long as Solution i is not dominated.
    while(j < solutions.size() && !(solutions[i] < solutions[j])){

      // If the solution has not been explored yet.
      if(!visited_solutions[j]){

	// If the solution I dominates the solution J :
	// it means the solutions J can not be Pareto optimal.
	if(solutions[i] > solutions[j]){
	  visited_solutions[j] = true;
	}else{
	  // Solution J is better on one attribute, but that's it.
	}
      }
      j++;
    }
    if(j >= solutions.size())
      result.push_back(solutions[i]);
  }
  return result;
}

std::vector<KnapsackSolution> Filter::online_filter(const std::vector<KnapsackSolution>& solutions, const KnapsackSolution& s, bool& improved){
  std::vector<KnapsackSolution> result;

  unsigned i = 0;
  while(i < solutions.size() && !(s < solutions[i])){
    if(!(solutions[i] < s))
      result.push_back(solutions[i]);
    i++;
  }
  
  improved = !(i < solutions.size());
  return i < solutions.size() ? solutions : result;
  
}
