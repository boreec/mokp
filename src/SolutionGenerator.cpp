#include "SolutionGenerator.hpp"

SolutionGenerator::SolutionGenerator(const Knapsack& k) : _knapsack(k){
  //
}

void SolutionGenerator::generate_randomly(const float probability, const unsigned quantity, std::vector<KnapsackSolution>& solutions){
  solutions.reserve(quantity);

  std::random_device rd;
  std::mt19937 gen(rd());
  std::bernoulli_distribution d(probability);
  const unsigned solution_size = _knapsack.get_containable_items_quantity();
  for(unsigned i = 0; i < quantity; ++i){
    std::vector<bool> solution;
    solution.reserve(solution_size);
    for(unsigned s = 0; s < solution_size; ++s){
      if(d(gen)){
	solution.push_back(true);
      }else{
	solution.push_back(false);
      }
    }
    solutions.push_back(KnapsackSolution(solution, _knapsack));
  }
}
