#ifndef __FILTER_HPP__
#define __FILTER_HPP__
#include <vector>
#include "Knapsack.hpp"
#include "KnapsackSolution.hpp"

/*
 * This class filters out Knapsack solutions to keep only the ones of interest.
 * */
class Filter{
public:
  /*
   * This method keeps only the solutions that do not exceed the weight capacity of a knapsack.
   * */
  static std::vector<KnapsackSolution> remove_overweight(const std::vector<KnapsackSolution>& solutions);

  /*
   * Remove the solutions with the same bit vector and same total profit and total weight..
   * */
  static std::vector<KnapsackSolution> remove_duplicates(const std::vector<KnapsackSolution>& solutions);
  /*
   * This method keeps only the pareto optimal solutions.
   * */
  static std::vector<KnapsackSolution> keep_pareto_optimal(const std::vector<KnapsackSolution>& solutions);

  static std::vector<KnapsackSolution> online_filter(const std::vector<KnapsackSolution>& solutions, const KnapsackSolution& s, bool& improved);
};
#endif
