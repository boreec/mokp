#ifndef __HILL_CLIMBING_HPP__
#define __HILL_CLIMBING_HPP__
#include <vector>
#include "Filter.hpp"
#include "KnapsackSolution.hpp"
class HillClimbing{
public:
  static std::vector<KnapsackSolution> climb(std::vector<KnapsackSolution>& base, const unsigned iterations);
};
#endif
