#include "Item.hpp"

Item::Item(const unsigned w, const unsigned p) : _w(w), _p(p) {
  //
}

unsigned Item::get_weight() const {
  return _w;
}

unsigned Item::get_profit() const {
  return _p;
}

std::string Item::to_string() const {
  return "w : " + std::to_string(_w) + " | p : " + std::to_string(_p);
}

