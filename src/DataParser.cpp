#include "DataParser.hpp"

void DataParser::parse(const std::string& filename, std::vector<Knapsack>& k){
  std::ifstream file(filename);

  if(!file.is_open()){
    throw std::runtime_error("Error occured during file opening on : " + filename);
  }

  std::string line;
  unsigned line_number = 0;
  unsigned total_knapsacks = 0;
  unsigned total_items = 0;
  unsigned item_w = 0, item_p = 0; // Item attributes
  while (getline(file,line)){
    if(!line_number){
      extract_headline_information(line, total_knapsacks, total_items);
      k.reserve(total_knapsacks);
    }else{
      if(line.find("knapsack",0) != std::string::npos){
	k.push_back(Knapsack());
	k.back().reserve_containable_items(total_items);
	item_w = 0;
	item_p = 0;
      }else if(line.find("capacity") != std::string::npos){
	k.back().set_capacity(extract_positive_value(line));
      }else if(line.find("weight",0) != std::string::npos){
	item_w = extract_positive_value(line);
      }else if(line.find("profit",0) != std::string::npos){
	item_p = extract_positive_value(line);
      }else{
	if(item_p && item_w){
	  k.back().add_containable_item(Item(item_w,item_p));
	  item_p = 0;
	  item_w = 0;
	}
      }
    }
    line_number++;
  }
  if(k.size() && item_p && item_w){
    k.back().add_containable_item(Item(item_w,item_p));    
  }
  file.close();
}

void DataParser::extract_headline_information(std::string& line, unsigned& knapsacks, unsigned& items){

  // Remove potential brackets that could mess up the split.
  std::size_t bracket_pos = line.find("(", 0);
  if(bracket_pos  != std::string::npos){
    line.erase(bracket_pos,1);
  }

  // Transform line into words and pick the word appearing before "knapsack" and "items".
  std::string previous_word = "", word;
  std::istringstream ss(line);
  while(ss >> word){
    if(word.find("knapsack", 0) != std::string::npos && previous_word != ""){
      knapsacks = std::stoul(previous_word);
    }
    if(word.find("items", 0) != std::string::npos && previous_word != ""){
      items = std::stoul(previous_word);
    }
    previous_word = word;
  }

  if(!knapsacks){
    throw std::runtime_error("The amount of knapsacks could not be found on the first line of data file.");
  }

  if(!items){
    throw std::runtime_error("The amount of items could not be found on the first line of data file.");
  }
}

unsigned DataParser::extract_positive_value(std::string& line){
  unsigned result = 0;
  
  // Remove potential brackets that could mess up the split.
  std::size_t plus_pos = line.find("+", 0);
  if(plus_pos  != std::string::npos){
    line.erase(plus_pos,1);
  }

  // Retrieve the positive value from the last word.
  std::string word;
  std::istringstream ss(line);

  // Ignore every word until the very last.
  while(ss >> word){}

  result = std::stoul(word);
  
  return result;
}
