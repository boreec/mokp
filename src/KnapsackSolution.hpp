#ifndef __KNAPSACK_SOLUTION_HPP__
#define __KNAPSACK_SOLUTION_HPP__
#include <string>
#include <vector>
#include "Knapsack.hpp"
/*
 * This class represents a solution to solve the Knapsack problem.
 * It is simply a vector of bits of size N, where N is the amount
 * of items the knapsack can contain.
 * No verification is made internally to check if the solution is
 * correct or not (if the knapsack's weight capacity is exceeded).
 * */
class KnapsackSolution{
private:
  /*
   * The solution itself. There are more efficient ways to store a bit
   * vector than using std::vector<bool>, but it's easier to use and to
   * understand for all.
   * */
  std::vector<bool> _solution;

  const Knapsack& _knapsack;

  unsigned _profit;

  unsigned _weight;
  
public:

  /*
   * Build a Knapsack's solution from a given bit vector.
   * */
  KnapsackSolution(const std::vector<bool>& solution, const Knapsack& k);
  
  /*
   * Return the current solution a string.
   * The string contains the weight and the profit of the solution.
   * */
  std::string to_string() const;

  const Knapsack& get_knapsack() const;

  unsigned get_profit() const;

  unsigned get_weight() const;

  bool exceed_capacity() const;

  std::vector<bool> get_solution() const;
  /*
   * Return true if k1 is a worst solution than k2 :
   * k1's weight is greater than k2's weight and
   * k1's profit is lesser than k2's profit.
   * */
  friend bool operator< (const KnapsackSolution& k1, const KnapsackSolution& k2);

  /*
   * Return true if k1 is a best solution than k2 :
   * k1's weight is lesser than k2's weight and
   * k1's profit is greater than k2's profit.
   * */
  friend bool operator> (const KnapsackSolution& k1, const KnapsackSolution& k2);

  /*
   * Compare two KnapsackSolutions k1 and k2 and return true if there are equal.
   * Two solutions are assumed to be equal if they have the exact same profit,
   * same weight and same bit vector.
   * */
  friend bool operator== (const KnapsackSolution& k1, const KnapsackSolution& k2);

  /*
   * The complete opposite of == function.
   * */
  friend bool operator!= (const KnapsackSolution& k1, const KnapsackSolution& k2);

};
#endif
