#ifndef __DATA_PARSER_HPP__
#define __DATA_PARSER_HPP__
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>
#include "Item.hpp"
#include "Knapsack.hpp"

/*
 * This class is used to parse the data from instance files.
 * It has been designed to work exclusively with a restricted
 * amount of formats.
 * */
class DataParser{
private:
  /*
   * Attempt to retrieve the knapsacks quantity and the items quantity from words in the first line
   * of the file. If a word "knapsack" is found, the word right before is converted to an unsigned
   * integer and used as the knapsack quantity. The same procedure is used for the items quantity.
   * */
  static void extract_headline_information(std::string& line, unsigned& knapsacks, unsigned& items);

  /*
   * Attempt to retrieve a positive value from a line. If a '+' char is found, it is erased from
   * the string. The positive value is the last unsigned found on the given line. If no positive
   * value has been found, 0 is returned.
   * */
  static unsigned extract_positive_value(std::string& line);
public:
  /*
   * Open a file and attempt to read the content to store the data into knapsacks.
   * If the attempt fails, a runtime exception is thrown.
   * */
  static void parse(const std::string& filename, std::vector<Knapsack>& k);
};
#endif
