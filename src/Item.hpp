#ifndef __ITEM_HPP__
#define __ITEM_HPP__
#include <string>
/*
 * This class represents an item that can be put or not
 * into a knapsack.
 * It has 2 attributes : a weight and a profit.
 * */
class Item{
private:
  unsigned _w;
  unsigned _p;
public:
  /*
   * Construct an item with a given weight and profit.
   * */
  Item(const unsigned w, const unsigned p);

  /*
   * Return the item's weight.
   * */
  unsigned get_weight() const;

  /*
   * Return the item's profit.
   * */
  unsigned get_profit() const;

  std::string to_string() const;
};
#endif
