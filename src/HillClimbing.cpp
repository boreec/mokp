#include "HillClimbing.hpp"
#include <iostream>
std::vector<KnapsackSolution> HillClimbing::climb(std::vector<KnapsackSolution>& base, const unsigned iterations_max){
  bool archive_improved = true;
  std::vector<KnapsackSolution> new_archive;
  std::vector<KnapsackSolution> best_archive = base;
  unsigned iteration = 0;
  while(archive_improved && iteration < iterations_max){
    archive_improved = false;
    std::cout << "improved!" << std::endl;
    for(const auto& solution : best_archive){
      std::vector<bool> bit_vector = solution.get_solution();

      for(unsigned i = 0; i < bit_vector.size(); ++i){
	for(unsigned j = i + 1; j < bit_vector.size(); ++j){
	  std::vector<bool> tmp_vector = bit_vector;
	  tmp_vector[i] = !tmp_vector[i];
	  tmp_vector[j] = !tmp_vector[j];

	  KnapsackSolution ks(tmp_vector, solution.get_knapsack());
	  if(ks > solution){
	    new_archive = Filter::online_filter(new_archive, ks, archive_improved);
	  }
	}
      }
    }
    if(archive_improved)
      best_archive = new_archive;
    iteration++;
  }
  return best_archive;
}
