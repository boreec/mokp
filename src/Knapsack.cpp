#include "Knapsack.hpp"

Knapsack::Knapsack(const unsigned capacity) : _capacity(capacity){
  //
}

unsigned Knapsack::get_capacity() const{
  return _capacity;
}

void Knapsack::set_capacity(const unsigned capacity){
  _capacity = capacity;
}

void Knapsack::add_containable_item(const Item& i){
  _containable_items.push_back(i);
}

std::string Knapsack::to_string() const{
  std::string result = "";
  result += "capacity : " + std::to_string(_capacity) + "\n";
  result += "containable items (" + std::to_string(_containable_items.size()) + ") : \n";
  for(const auto& i : _containable_items){
    result += i.to_string() + "\n";
  }
  return result;
}

void Knapsack::reserve_containable_items(const unsigned item_quantity){
  _containable_items.reserve(item_quantity);
}

Item Knapsack::get_containable_item(const unsigned i) const{
  return _containable_items[i];
}

unsigned Knapsack::get_containable_items_quantity() const{
  return _containable_items.size();
}
