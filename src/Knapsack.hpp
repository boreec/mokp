#ifndef __KNAPSACK_HPP__
#define __KNAPSACK_HPP__
#include <string>
#include <vector>
#include "Item.hpp"
/*
 * This class represents a 0/1 knapsack.
 * It is made of a set of items, weight and profit associated
 * with each items, and an uppoer found for the capacity of the
 * knapsack (the total weight does not exceed the capacity).
 * */
class Knapsack{
private:

  /*
   * The upper bound for the total weight of the knapsack.
   * */
  unsigned _capacity;

  /*
   * Store every possible items that can fit in the knapsack. This
   * container does not actually store them, it's just a representation
   * of every items associated to this knapsack.
   * A vector is used over a set because two items with the same weight
   * and the same profit can be in the knapsack.
   * */
  std::vector<Item> _containable_items;
public:

  /*
   * Construct a Knapsack object with a given capacity.
   * */
  Knapsack(const unsigned capacity = 0);

  /*
   * Return the knapsack's capacity.
   * */
  unsigned get_capacity() const;

  /*
   * Set the knapsack's capacity.
   * */
  void set_capacity(const unsigned c);

  /*
   * Add an item to the contaible category.
   * */
  void add_containable_item(const Item& i);
  
  /*
   * Return this object as a string containing the knapsack's capacity
   * and every items attributes.
   * */
  std::string to_string() const;

  /*
   * In case the amount of items is already known, the container can reserve
   * memory to prevent reallocating.
   * */
  void reserve_containable_items(const unsigned item_quantity);

  /*
   * Return the i-th containable item.
   * */
  Item get_containable_item(const unsigned i) const;

  /*
   * Return the amount of containable items in the knapsack?
   * */
  unsigned get_containable_items_quantity() const;
};
#endif
