#include "KnapsackSolution.hpp"

KnapsackSolution::KnapsackSolution(const std::vector<bool>& solution, const Knapsack& k) :
  _solution(solution), _knapsack(k){
  _weight = 0;
  _profit = 0;
  for(unsigned i = 0; i < _solution.size(); ++i){
    if(_solution[i]){
      _weight += _knapsack.get_containable_item(i).get_weight();
      _profit += _knapsack.get_containable_item(i).get_profit();
    }
  }
}

std::string KnapsackSolution::to_string() const{
  return "weight: " + std::to_string(_weight) + " profit: " + std::to_string(_profit); 
}

const Knapsack& KnapsackSolution::get_knapsack() const{
  return _knapsack;
}

unsigned KnapsackSolution::get_profit() const{
  return _profit;
}

unsigned KnapsackSolution::get_weight() const{
  return _weight;
}

bool KnapsackSolution::exceed_capacity() const{
  return _weight > _knapsack.get_capacity();
}

bool operator< (const KnapsackSolution& k1, const KnapsackSolution& k2){
  return k1._profit < k2._profit && k1._weight > k2._weight;
}

bool operator> (const KnapsackSolution& k1, const KnapsackSolution& k2){
  return k1._profit > k2._profit && k1._weight < k2._weight;
}

bool operator== (const KnapsackSolution& k1, const KnapsackSolution& k2){
  if(k1._profit != k2._profit || k1._weight != k2._weight || k1._solution.size() != k2._solution.size())
    return false;

  unsigned j = 0;
  while (j < k1._solution.size() && k1._solution[j] == k2._solution[j]){
    j++;
  }

  return j >= k1._solution.size();
}

bool operator!= (const KnapsackSolution& k1, const KnapsackSolution& k2){
  return !(k1 == k2);
}

std::vector<bool> KnapsackSolution::get_solution() const {
  return _solution;
}
