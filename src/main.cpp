#include <getopt.h>
#include <iostream>
#include <string>
#include "DataParser.hpp"
#include "Filter.hpp"
#include "Heuristic.hpp"
#include "HillClimbing.hpp"
#include "Item.hpp"
#include "Knapsack.hpp"
#include "SolutionGenerator.hpp"

void print_usage(const std::string& program_name){
  std::cerr << "Usage for program " << program_name << " :"  << std::endl;
  std::cerr << program_name << " -b UNSIGNED_VALUE : use a certain amount of solutions as base ones." << std::endl;
  std::cerr << program_name << " -d INSTANCE_FILE : load data from a file." << std::endl;
  std::cerr << program_name << " -D : export solutions to .dat and .gnu files." << std::endl;
  std::cerr << program_name << " -f FILTER : filter out base solutions to some characteristics." << std::endl;
  std::cerr << program_name << " -h : display this usage message." << std::endl;
  std::cerr << program_name << " -M UNSIGNED_VALUE : set a limit for iterations for heuristics." << std::endl;
  std::cerr << program_name << " -p : display data loaded from file if any into stdout." << std::endl;
}

/*
 * For every knapsacks loaded, the solutions will be outputted in a corresponding dat file.
 * The filename has format "sol_X.dat" where X is the current index on knapsacks.
 * In all of the dat files, the first line contains a comment with the Weight/Profit.
 * Other lines contains a solution's weight and a solution's profit seperated by
 * a tab character.
 * Then another corresponding file "sol_X.gnu" is generated and can be used by gnuplot
 * to plot the results.
 * */
void export_solutions_to_dat(const std::vector<KnapsackSolution>& solutions, const std::vector<Knapsack>& knapsacks){
  for(unsigned k = 0; k < 1; ++k){
    const std::string dat_filename = "sol_" + std::to_string(k) + ".dat";
    const std::string gnu_filename = "sol_" + std::to_string(k) + ".gnu";
    std::ofstream dat_file(dat_filename);

    if(!dat_file.is_open()){
      throw std::runtime_error("Error occurred while attempting to write solutions into " + dat_filename);
    }

    dat_file << "# Weight\tProfit\n";

    for(const auto& s : solutions){
      unsigned w = s.get_weight();
      unsigned p = s.get_profit();
      dat_file << std::to_string(w) << "\t" << std::to_string(p) << "\n";
    }
    dat_file.close();

    std::ofstream gnu_file(gnu_filename);

    if(!gnu_file.is_open()){
      throw std::runtime_error("Error occurred while attempting to write " + gnu_filename);
    }
    gnu_file << "set title \""<< std::to_string(solutions.size()) << " MOKP solutions\"\n";
    gnu_file << "set xlabel \"Knapsack's weight\"\n";
    gnu_file << "set ylabel \"Knapsack's profit\"\n";
    gnu_file << "plot '" << dat_filename << "' with points\n";
    gnu_file.close();
  }
}

int parseArgs(int argc, char** argv){
  std::string data_file = "";
  std::string heuristic = "";
  unsigned base_solutions = 10;
  unsigned heur_iterations = 20;
  bool print_data = false;
  bool export_dat_file = false;
  bool filter_duplicate = false;
  bool filter_overweight = false;
  bool filter_pareto_optimal = false;
  int opt;
  
  while ((opt = getopt(argc, argv, "b:Dd:f:hM:p")) != -1) {
    switch (opt) {
    case 'b':
      base_solutions = std::stoul(optarg);
      break;
    case 'd':
      data_file = std::string(optarg);
      break;
    case 'D':
      export_dat_file = true;
      break;
    case 'f':
      if(std::string(optarg) == "OVERWEIGHT"){
	filter_overweight = true;
      }
      if(std::string(optarg) == "PARETO_OPTIMAL"){
	filter_pareto_optimal = true;
      }
      if(std::string(optarg) == "DUPLICATE"){
	filter_duplicate = true;
      }
      break;
    case 'h':
      print_usage(std::string(argv[0]));
      break;
    case 'M':
      heur_iterations = std::stoul(optarg);
      break;
    case 'p':
      print_data = true;
      break;
    default: /* '?' */
      print_usage(std::string(argv[0]));
      return EXIT_FAILURE;
    }
  }

  if(data_file == ""){
    std::cerr << "A data file is required." << std::endl;
    return EXIT_FAILURE;
  }

  std::vector<Knapsack> knapsacks;
  DataParser::parse(data_file, knapsacks);

  if(print_data){
    for(const auto& k : knapsacks){
      std::cout << k.to_string() << std::endl;
    }
  }

  // Generate solutions randomly as a base for heuristics.
  std::vector<KnapsackSolution> solutions;
  SolutionGenerator g(knapsacks[0]);
  g.generate_randomly(0.5, base_solutions, solutions);
  

  if(filter_overweight){
    solutions = Filter::remove_overweight(solutions);
  }

  if(filter_duplicate){
    solutions = Filter::remove_duplicates(solutions);
  }
  
  if(filter_pareto_optimal){
    solutions = Filter::keep_pareto_optimal(solutions);
  }

  solutions = HillClimbing::climb(solutions,heur_iterations);
  
  if(export_dat_file){
    export_solutions_to_dat(solutions, knapsacks);
  }
  return EXIT_SUCCESS;
}

int main(int argc, char** argv){
  return parseArgs(argc, argv);
}
